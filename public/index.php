<?php
# Main file to be used for calling out the core controller. We will load the
# the controller from here.
# We also define some important constant to be used during the front controller
# initialization.
# NOTES
# -----
# Do not include trailing slash for the end of every value !

# Added by @sanusi on 04-02-2014
$timezone = "Asia/Kuala_Lumpur";

# This section define all the required variables
# Application status [ development, production ]
$environment = "development";

# Define core system location or use default. Default will point to the system folder in top of this folder
# System location [ system, {defined} ] where `system` is the default location and {defined} if you are putting
# the system library outside the default installation
$system_root = "system";

# Define the application folder location which contains the application itself. You can change the application folder
# name to whatever your preferences is
$application = "application";

$get_route = "request";

########################################### - DO NOT EDIT THIS LINE - #########################################
# Set the application environment
define("ENVIRONMENT",$environment);

# Let set our application environment status
if(defined("ENVIRONMENT"))
{
    switch(ENVIRONMENT)
    {
        case "development":
            error_reporting(E_ALL ^ E_NOTICE);
            break;
        case "production":
            error_reporting(0);
            break;
        case "all":
            error_reporting(-1);
            break;
        default:
            exit("<pre>Invalid environment setting.<br />Please set to either development or production.</pre>");
    }
}

# Set the timezone
date_default_timezone_set($timezone);

# Get the ROOT path for the whole application
$root_path = str_replace("\\","/",dirname(dirname(__FILE__)))."/";

# Define this file name
define('SELF', pathinfo(__FILE__, PATHINFO_BASENAME));

# Define the system path and make sure the Windows and UNIX trailing slashes are 
# properly replace with UNIX/Linux type trailing slashes
switch ($system_root)
{
    case "system":
        $system_path = $root_path."/system";
        break;
    default :
        if(!is_dir($system_root))
        {
            exit( "<pre>Invalid system folder</pre>" );
        }
        else
        {
            $system_path = $system_root;
        }        
        break;
}
define("SYSTEM", $system_path."/");

# Define ROOT path for this application
define("BASEDIR",$root_path);

# Define Application path
define("APPPATH",$root_path.$application."/");
if(!is_dir(APPPATH))
{
    exit("<pre>Your application path is not set correctly.<br />Please correct it.</pre>");
}

# Define the Application Configuration constant
define("CONFIG",APPPATH."configs/");

# Define the Application Controllers constant
define("CONTROLLER",APPPATH."controllers/");

# Define the Application Models constant
define("MODEL",APPPATH."models/");

# Define the Application Views constant
define("VIEW",APPPATH."views/");

# Define the Application Libraries constant
define("LIBRARY",APPPATH."libraries/");

# Define the Application Templates constant
define("TEMPLATE",APPPATH."templates/");

# Define the Application System Core Library
define("CORE",SYSTEM."core/");

# Define the Application System Library
define("SYSLIB",SYSTEM."libraries/");

# Let's load the bootstrapper here
if( file_exists (SYSTEM.'core/CoreCFX.php'))
{
    require_once SYSTEM.'core/CoreCFX.php';
}
else
{
    die ("Main bootstrap file can't be found. Please check your system folder settings");
}
