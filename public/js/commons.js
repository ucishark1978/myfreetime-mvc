function windowOpen(url,name)
{
	new_window = window.open(url,name, 'scrollbars=1, resizable,dependent,status,width=800,height=300,left=10,top=10')
}

function confirmbox(url,url1)
{
	var answer = confirm ("Confirm you action?");
	if (!answer)
	{
		window.location=url1;
	}	
	else
	{
		window.location=url;
	}
		
}

function _checkpass(obj, obj1) {
        if (obj.value != obj1.value) {
                alert("Fields does not match");
                obj.value = "";
                obj1.value = "";
                obj1.focus();
        }
}

function goURL(url)
{
    return top.location.replace(url);
}

function changeClass(id,classname){
    document.getElementById(id).setAttribute("class", classname);
}

function letternumber(e,type)
{
    var key;
    var keychar;

    if (window.event)
    key = window.event.keyCode;
    else if (e)
    key = e.which;
    else
    return true;
    keychar = String.fromCharCode(key);
    keychar = keychar.toLowerCase();

    // given type
    if(type=="numeric"){
        stringallowed = "0123456789";
    } else if (type=="alphanum"){
        stringallowed = "abcdefghijklmnopqrstuvwxyz0123456789";
    } else {
        stringallowed = "abcdefghijklmnopqrstuvwxyz";
    }
    
    // control keys
    if ((key==null) || (key==0) || (key==8) || 
        (key==9) || (key==13) || (key==27) || (key==32))
    return true;

    // alphas and numbers
    else if (((stringallowed).indexOf(keychar) > -1))
    return true;
    else
    return false;
}

function usernametype(e)
{
    var key;
    var keychar;

    if (window.event)
    key = window.event.keyCode;
    else if (e)
    key = e.which;
    else
    return true;
    keychar = String.fromCharCode(key);
    keychar = keychar.toLowerCase();

    // given type
        stringallowed = "abcdefghijklmnopqrstuvwxyz0123456789_";
    
    // control keys
    if ((key==null) || (key==0) || (key==8) || 
        (key==9) || (key==13) || (key==27))
    return true;

    // alphas and numbers
    else if (((stringallowed).indexOf(keychar) > -1))
    return true;
    else
    return false;
}