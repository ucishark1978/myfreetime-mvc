<?php
if (!defined('BASEDIR')) exit('<code>Forbidden Access</code>');
$memory_usage = memory_get_usage(true);
$usage = $memory_usage/1024/1024;
?>
Total memory usage : <span style='color: greenyellow'><?php echo $usage ?></span> MB
