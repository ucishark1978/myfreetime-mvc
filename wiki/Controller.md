Go back to [Main Content](https://bitbucket.org/ucishark1978/php-mvc/wiki/Introduction%20to%20PHP-MVC)
* * *
# Controller #
Controller is used to control all the request related and passed it back to either View or the Model. The naming convention is based on what you preferred but the class name should be the same as the file name. Example as below:-

Filename : shipment.php

The class should be as follow:-
```
#!php

class shipment extends Controller {

  function index() # you need this as to replace the index.php when calling the URL. This is should be your first function in this controller
  {
      # your code goes here
  }
}

```
* * *
As we go from here, let us start on how easy to code in this controller and what you need to access inside your browser. Below are the details explained.

URL : http://localhost:8080/myapp/shipment/get_all/ships

This is how we defined it:-

URL : http://localhost:8080/myapps/[controller]/[method]/URL_3/URL_4/URL_5/URL_6/URL_7/URL_8/....URL_N

As you need to access the variable in the URL, it is automatically create the CONSTANT value for each and every slash that you put in the browser URL. The controller is in URL_1 and the method is in URL_2

Look at the code below for the shipment controller:-
```
#!php

class shipment extends Controller {

  function index() 
  {
      $this->get_all();
  }

  function get_all()
  {
     $type = URL_3;
     $data = $this->Shipment->getAll($type);
     $this->set("data",$data); # assign a variable with its value to be rendered in the view
     $this->loadview("shipment/display-all",TRUE); 
  }
}

```
The $this->loadview() is used to return which view that need to be displayed. When you set it to TRUE, it will automatically created the display-all.php in the shipment folder provided the access to the application folder are writable.

