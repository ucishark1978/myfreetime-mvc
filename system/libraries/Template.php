<?php
class Template
{
    // member definition
    var $output;

    function __construct($templateFile = "default.html")
    {
    	(file_exists($templateFile)) ? $this->output = file_get_contents($templateFile) : "Not found";
    }

 
    function parseTemplate($tags=array())
    {
    	if(count($tags) > 0 )
    	{
    		foreach ($tags as $tag => $data)
    		{
    			$data = (file_exists($data)) ? $this->parseFile($data) : $data;
    			$this->output=str_replace('{'.$tag.'}',$data,$this->output);
    		}
    	}
    	else
    	{
    		die (load_msg("Template does not have any tags for replacement","msg"));
    	}
    }

    function parseFile($file)
    {
    	ob_start();
    	include($file);
    	$content=ob_get_contents();
    	ob_end_clean();
    	return $content;
    }
    
    function display()
    {
      return $this->output;
    }
    
    function loadtemplate($tags)
    {
    	$this->parseTemplate($tags);
    	echo $this->display();    	
    }
}
