<?php
if (!defined('BASEDIR')) exit('<code>Forbidden Access</code>');

# Commons function used in this application on the front controller

# debug output function
if (!function_exists('load_msg'))
{
    function load_msg($string,$type="debug")
    {
        $err = array("dbg"=>"DEBUG","msg"=>"MESSAGE","sql"=>"SQL","view"=>"VIEW");
        return "<div style='z-index: 100; position: relative; top: 0px; left: 0px; color: #F3F3F3; font-size:12px; width:99%; padding: 7px; background-color: #666; border-bottom: 1px solid #E5E5E5; font-family: Arial'>$err[$type] # $string<br /></div>";  
    }
}

# Bootstrapper functions
function url_helper($page1)
{
    require_once SYSTEM."libraries/htmlpurify/HTMLPurifier.standalone.php";
    $config = HTMLPurifier_Config::createDefault();
    $purifier = new HTMLPurifier($config);
    $p1leninput = explode("/", $page1);
    //print_r($p1len);
    $p1len = filter_var_array($p1leninput,FILTER_SANITIZE_SPECIAL_CHARS);
    $page2 = $p1len;
    $total = count($p1len);

    # set automatic define
    for($i=0;$i<=$total;$i++)
    {
        $urlNum = $i+1;
        define("URL_".$urlNum,$page2[$i]);
    }

    # if only detects URL_1 then load index.php else load next URL_2
    
    if(URL_1)
    {
        ob_start();
        if(URL_2)
        {
            if(file_exists(VIEW.URL_1."/".URL_2.EXT))
            {
                include (VIEW.URL_1."/".URL_2.EXT);
            }
        }
        else
        {
            if(file_exists(VIEW.URL_1."/index".EXT))
            {
                include (VIEW.URL_1."/index".EXT);
            }
        }
    }    
        $content = ob_get_contents();
        $clean_html = $purifier->purify($content);
        ob_end_clean();
        print $clean_html;
}

# Simple callHook for the rest of the application
function callHook($default){
    $uri1 = filter_var(URL_1,FILTER_SANITIZE_SPECIAL_CHARS);
    $uri2 = filter_var(URL_2,FILTER_SANITIZE_SPECIAL_CHARS);
    //$uri1 = URL_1;
    //$uri2 = URL_2;
    
    if($uri1 == NULL)
    {
        $classname = $default;
    }
    else
    {
        $classname = $uri1;
    }
    $method = $uri2;
    try {
        if( ! class_exists( $classname ) ) {
            throw new Exception(load_msg("The requested page $classname is not loaded / found.","msg"));
            exit();
        }
        $var = new $classname();
       
            if ( $uri2 == NULL )
            {
                $var->index();
            }
            else
            {
                if(!method_exists($var, $method))
                {
                    throw new Exception(load_msg("The requested page $method not found","mmsg"));
                    exit();
                }
                $var->$method();                
            }
        }
    
    catch( Exception $e ) {
        echo $e->getMessage();
    }
}

# Search for Magic Quotes and remove them
function stripSlashesDeep($value) {
    $value = is_array($value) ? array_map('stripSlashesDeep', $value) : stripslashes($value);
    return $value;
}

# Remove Magic Quotes function
function removeMagicQuotes() {
    if ( get_magic_quotes_gpc() ) {
            $_GET    = stripSlashesDeep($_GET   );
            $_POST   = stripSlashesDeep($_POST  );
            $_COOKIE = stripSlashesDeep($_COOKIE);
    }
}

# view loader to load view required in the controller
function loadview($viewname,$data=array()){
    if(file_exists(VIEW.$viewname.".php")){
        include VIEW."$viewname.php";
    } else {
        load_msg("View $viewname Not Found","msg");
    }
}

# function to load the controller, model and libraries
function &load_apps_class($type, $configvar){
    
    static $_classes = array();
    
    try{
        if(is_array($configvar))
        {
            foreach($configvar as $filename)
            {
                if(!file_exists($type.$filename.".php"))
                {
                    throw new Exception();
                    exit(load_msg("Invalid configuration files for the application. Please check", "msg"));
                }
                else
                {
                    require_once $type.$filename.".php";
                }
            }
        }
        else
        {
            if(!file_exists($type.$configvar.".php"))
            {
                exit(load_msg("Not found - $type$configvar.php","msg"));
            }
            else
            {
                $filename = $configvar;
                require_once $type.$configvar.".php";
            }
        }
        is_loaded($filename);
        $_classes[$filename] = new $filename();
        return $_classes[$filename];
    }
    catch ( Exception $e )
    {
        echo $e->getMessage();
    }    
}

# This function is to keep track what has been loaded
if ( ! function_exists('is_loaded'))
{
	function is_loaded($class = '')
	{
		static $_is_loaded = array();

		if ($class != '')
		{
			$_is_loaded[$class] = $class;
		}

		return $_is_loaded;
	}
}

# function load the template routing controller
function template_route($page)
{
    ob_start();
    if(!file_exists(VIEW.$page.".php"))
    {            
        exit(load_msg("Template Controller file not found", "msg"));
    }
    else
    {
        include VIEW.$page.".php";
    }
    $content = ob_get_contents();
    ob_end_clean();
    print $content;
}

# function to load the template name
function load_template($template,$data=array())
{
    if(class_exists('Template'))
    {
        $tp = new Template(TEMPLATE.$template);
        return $tp->loadtemplate($data);
    }
}

# Function to set the Base URL
function base_url(){
        if (isset($_SERVER['HTTP_HOST']))
            {
                $base_url = isset($_SERVER['HTTPS']) && strtolower($_SERVER['HTTPS']) !== 'off' ? 'https' : 'http';
                $base_url .= '://'. $_SERVER['HTTP_HOST'];
                $base_url .= str_replace(basename($_SERVER['SCRIPT_NAME']), '', $_SERVER['SCRIPT_NAME']);
                $base .= str_replace("public/", "", $base_url);
            }
            else
            {
                $base = 'http://localhost/';
            }
            return $base;
    }
    
if ( ! function_exists('load_class'))
{
	function &load_class($class, $directory = 'libraries')
	{
		static $_classes = array();

		// Does the class exist?  If so, we're done...
		if (isset($_classes[$class]))
		{
			return $_classes[$class];
		}

		$name = FALSE;

		// Look for the class first in the local application/libraries folder
		// then in the native system/libraries folder
		foreach (array(CONTROLLER, SYSLIB, MODEL, LIB) as $path)
		{
			if (file_exists($path.$directory.'/'.$class.'.php'))
			{
				$name = $prefix.$class;

				if (class_exists($name) === FALSE)
				{
					require($path.$directory.'/'.$class.'.php');
				}

				break;
			}
		}

		// Did we find the class?
		if ($name === FALSE)
		{
			// Note: We use exit() rather then show_error() in order to avoid a
			// self-referencing loop with the Excptions class
			exit('Unable to locate the specified class: '.$class.'.php');
		}

		// Keep track of what we just loaded
		is_loaded($class);

		$_classes[$class] = new $name();
		return $_classes[$class];
	}
}

# function to return the value of the loaded classes
function cfg_loaded_class()
{

    # Get the config file in
    include CONFIG."application.php";
    
    # get the array list for the loaded controllers
    $controllers = $apps['controllers'];
    $models = $apps['models'];
    $libraries = $apps['libraries'];
    $sys_libraries = $apps['system_libraries'];
        
    $controlkeys = array_fill_keys($controllers,'CONTROLLER');
    $modelkeys = array_fill_keys($models,'MODEL');
    $librarieskeys = array_fill_keys($libraries, 'LIBRARY');
    $sys_librarieskeys = array_fill_keys($sys_libraries, 'SYSLIB');
    
    $class = array_merge($controlkeys,$modelkeys, $librarieskeys, $sys_librarieskeys);
    
    return $_class[$class];
}