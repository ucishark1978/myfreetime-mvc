<?php
if (!defined('BASEDIR')) {
    exit('<code>Forbidden Access</code>');
}

# Class : Model
# Type : Core System
# Author : Sanusi Zainol Abidin
# Date : 15/03/2012

class Model extends DB {
    
    var $_debug = FALSE;
    var $_columns = array("*");
    var $_conditions = array("");
    var $_return_id = "";
    
    public function exec_query($sql, $type = "QUERY", $table = "")
    {
        switch($type)
        {
            case "FETCH":
                $result = $this->pdo_select($sql);
                break;
            case "DEBUG":
                $result = $this->pdo_debug($sql);
                break;
            case "INSERT":
                $result = $this->pdo_insert($sql);
                break;
            case "INSERT_DEBUG":
                $result = $this->pdo_insert($sql,DEBUG);
                break;
            default:
                $result = $this->pdo_query($sql);
                break;
        }
        return $result;
    }
    
    public function findAll()
    {
        $table = get_called_class();
        $tbl = strtolower($table);
        $sql = "SELECT ";
        foreach($this->_columns as $col)
        {
            $sql .= "$col,";
        }
        $sql2 = rtrim($sql,",");
        $sql2 .= " FROM $tbl";
        
        if($this->_debug === TRUE)
        {
            return $this->exec_query($sql2,DEBUG);
        }
        else
        {
            return $this->exec_query($sql2,FETCH);
        }
    }
    
    # this is a function that you can do for JOIN
    public function find()
    {
        if(!empty($this->_conditions) && is_array($this->_conditions))
        {
            $table = get_called_class();
            $tbl = strtolower($table);
            $sql = "SELECT ";
            foreach($this->_columns as $col)
            {
                $sql .= "{$col},";
            }
            $sql2 = rtrim($sql,",");
            $sql2 .= " FROM $tbl";
             
            $sql2 .= $this->_find_conditions();
        }
        return $this->enable_debug($sql2);
    }
    
    private function _find_conditions()
    {
        foreach($this->_conditions as $sqlConditions => $sqlValue)
        {
            if(!is_array($sqlValue))
            {
                $sql2 .= " $sqlConditions $sqlValue ";
            }
            else
            {
                $sql2 .= $this->_find_iterate_conditions($sqlConditions, $sqlValue);
            }
        }
        return $sql2;
    }
    
    private function _find_iterate_conditions($sqlConditions,$sqlValue)
    {
        foreach($sqlValue as $doValue)
        {
            $sql2 .= " $sqlConditions $doValue ";
        }
        return $sql2;
    }
    
    private function enable_debug($sql2)
    {
        if($this->_debug === TRUE)
        {
            return $this->exec_query($sql2,DEBUG);
        }
        else
        {
            return $this->exec_query($sql2,FETCH);
        }
    }
    
    # get data by id
    public function findbyid($id,$id_col_name="id")
    {
        $table = get_called_class();
        $tbl = strtolower($table);
        $sql = "SELECT * FROM $tbl WHERE $id_col_name = $id";
        return $this->exec_query($sql,FETCH);
    }
    
    # create new record
    # usage in Model:
    # $array = array ("col_name" => "val_name", "col_address" => "val_address");
    # $this->create($array);    
    #
    public function create($array)
    {
        $table = get_called_class();
        $tbl = $tbl = strtolower($table);
        if(is_array($array))
        {
            $data = $this->create_insert_record($tbl,$array);
            if($this->_debug === TRUE)
            {
                $this->exec_query($data,DEBUG);
            }
            else
            {
                if($this->_return_id == "") {
                     return $this->execute_insert($array, $data);
                } else {
                     return $this->execute_insert($array, $data,$this->_return_id);
                }
            }
        }
        else
        {
            exit();
        }
    }
    
    # update record
    # usage in Model:
    # $array = array ("col_name" => "val_name", "col_address" => "val_address");
    # $this->update($array);    
    #
    public function update($array)
    {
        $table = get_called_class();
        $tbl = $tbl = strtolower($table);
       
        if(is_array($array))
        {
            $data = $this->update_record($tbl,$array);
            if($this->_debug === TRUE)
            {
                $this->exec_query($data,DEBUG);
            }
            else
            {
                return $this->execute_update($array, $data);
            }
        }
        else
        {
            exit();
        }
    }
    
    # create record
    private function create_insert_record($tbl, $array)
    {
        $table_columns = array_keys($array);
        $data = "INSERT INTO {$tbl} (";
        foreach($table_columns as $cols)
        {
            $datacols .= $cols.", ";
        }
        $data .= rtrim($datacols,", ") . ") VALUES (";
        foreach($array as $keys=>$vals)
        {
            $datavals .= ":{$keys}" . ", ";
        }
        $data .= rtrim($datavals,", ") . ") ". $this->_return_id;
        return $data;
    }
    
    # function to execute insert using prepared INSERT statement bindParam function
    private function execute_insert($array,$data)
    {
        $sql_prep = $this->db()->prepare($data);
        $sql_prep->execute($array);
        if($return_id == ""){
            $result = $sql_prep->fetch(PDO::FETCH_ASSOC);
        } else {
            $result = $sql_prep->fetchAll();
        }
        return $result;
    }
    
    # update record
    private function update_record($tbl, $array)
    {
        if(!empty($this->_conditions) && is_array($this->_conditions))
        {
            $conditions_update = $this->_find_conditions();
        }
        else
        {
            $conditions_update = "";
        }
        $data = "UPDATE {$tbl} SET ";
        foreach($array as $keys=>$vals)
        {
            $datavals .= "$keys = :{$keys}" . ", ";
        }
        $data .= rtrim($datavals,", ") . " ". $conditions_update;
        return $data;
    }
    
    # function to execute update using prepared INSERT statement bindParam function
    private function execute_update($array,$data)
    {
        $sql_prep = $this->db()->prepare($data);
        $sql_prep->execute($array);
        return $sql_prep;
    }
    
    # function to do select
    private function pdo_select($sql)
    {
        $stmt = $this->db()->prepare($sql);
        $stmt->execute();
        while($row = $stmt->fetch(PDO::FETCH_ASSOC))
        {
            $data[] = $row;
        }
        return $data;
    }
    
    # function to perform normal query
    private function pdo_query($sql)
    {
        $stmt = $this->db()->prepare($sql);
        $result = $stmt->execute();
        return $result;
    }
    
    # function to do insert
    private function pdo_insert($sql,$debug="")
    {
        if($debug === "DEBUG")
        {
            $data_debug = $this->pdo_gen_sql($sql);
            $this->pdo_debug($data_debug);
        }
        else
        {
            if(is_array($sql))
            {
                # lets construct simple query
                $stmt = $this->pdo_gen_sql($sql);
                $data = $this->pdo_query($stmt);
            }
            else
            {
                $data = "None";
            }
        }
        return $data;
    }
    
    # generate INSERT statement
    private function pdo_gen_sql($sql)
    {        
        if(array_key_exists("cols", $sql) && array_key_exists("values", $sql) && array_key_exists("table", $sql))
        {
            $data = "INSERT INTO `{$sql['table']}` (";
            foreach($sql['cols'] as $cols)
            {
                $datacols .= $cols.",";
            }
            $data .= rtrim($datacols,",") . ") VALUES (";
            foreach($sql['values'] as $vals)
            {
                $datavals .= "\"".$vals."\",";
            }
             $data .= rtrim($datavals,",") . ")";
        }
        else
        {
            $data = $this->gen_sql_error($sql);
        }
        return $data;
    }
    
    # function display gen_sql_error
    private function gen_sql_error($sql)
    {
        if(!array_key_exists("table", $sql))
        {
            echo $this->gen_error(KEY_TABLE_MISSING);
        }
        if(!array_key_exists("cols", $sql))
        {
            echo $this->gen_error(KEY_COLS_MISSING);
        }
        if(!array_key_exists("values", $sql))
        {
            echo $this->gen_error(KEY_VALUES_MISSING);
        }
    }
    
    # function to display debug messages
    private function pdo_debug($sql)
    {
        echo "<div class=\"navbar-static-top\"><pre><span class='label label-info'>SQL DEBUG</span> >> <span class='text text-danger'>$sql</span></pre></div>";
    }
    
    # display msg error
    private function gen_error($type="")
    {
        switch($type)
        {
            case "KEY_TABLE_MISSING":
                $data = "<pre><span class='label label-danger'>ERR MODEL</span> >> Array key [table] is required.</pre>";                
                break;
            case "KEY_COLS_MISSING":
                $data = "<pre><span class='label label-danger'>ERR MODEL</span> >> Array key [cols] is required.</pre>";                
                break;
            case "KEY_VALUES_MISSING":
                $data = "<pre><span class='label label-danger'>ERR MODEL</span> >> Array key [values] is required.</pre>";
                break;
            default:
                break;
        }        
        return $data;
    }
}
