<?php
if (!defined('BASEDIR')) exit('<code>Forbidden Access</code>');

# This is our front controller to load every configuration and constant
global $get_route;
global $apps;

# CFX Version
define("CFX_VER","1.6");

# Load the Commons function file
require_once SYSTEM."core/Commons.php";

# Load our application configuration
require_once CONFIG."application.php";

# Load our database configuration
require_once CONFIG."database.php";

# Remove the magic quotes
removeMagicQuotes();

# Load the system libraries
$system_library = $apps['system_libraries'];
if($system_library)
{
    load_apps_class(SYSTEM."libraries/", $system_library);
}

# Since we are heavily depending on simple templating system, let's load the Template class
load_apps_class(SYSTEM."libraries/", "Template");

# Load the Core Controller
require_once SYSTEM."core/Controller.php";

# Load this if only defined inside the application configuration
# Load the Model Controller
if(in_array("DB", $apps['system_libraries']))
{
    require_once SYSTEM."core/Model.php";
}

# Loading the Front Controller
# Get default route and defined as ROUTE
$default_route = $apps['route'];
define("ROUTE",$default_route);

# Get the default route of your controller
if( !file_exists ( CONTROLLER.ROUTE.".php" ) )
{
    exit( load_msg ( "Your default controller setting is not correct" , "msg" ) );
}

# Load the default controller
include CONTROLLER.ROUTE.".php";

# Load URL Helper for easy URL definition
url_helper($_GET[$get_route]);

# Set the base url
$base_url = base_url();
define("BASE_URL",$base_url);

# Load all the controllers
$controller_lists_all = glob(CONTROLLER . "*.php");

# search the default route controller
$exclude_array = array_search(CONTROLLER.ROUTE.".php", $controller_lists_all);

# remove the default route controller from the include loop
unset($controller_lists_all[$exclude_array]);

foreach ($controller_lists_all as $var => $ctrlname)
{
    if(file_exists($ctrlname))
    {
        include $ctrlname;
    }
}

# Load all the models
$model_lists = glob(MODEL . "*.php");
foreach ($model_lists as $var => $classname)
{
    if(file_exists($classname))
    {
        load_apps_class(MODEL, str_replace(".php","",basename($classname)));
    }
}

# Load all the user's libraries
foreach ($apps['libraries'] as $value) {
    if(!file_exists( LIBRARY."$value.php"))
    {
        die ( load_msg("Missing libraries file. Re-check again your loaded controllers") );
    }
    include LIBRARY.$value.".php";
}

# Set the template controller to be loaded
# Get template controller filename and load the controller
$tpl_control = $apps['tpl_controller'];
load_apps_class(VIEW."pages/", $tpl_control);